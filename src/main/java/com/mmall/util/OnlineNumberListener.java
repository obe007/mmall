package com.mmall.util;

import com.mmall.common.Const;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class OnlineNumberListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        ServletContext application = event.getSession().getServletContext();

        Integer onlineNumber = (Integer) application.getAttribute("OnlineNumber");

        if (onlineNumber == null) {
            onlineNumber = 0;
        } else {
            ++onlineNumber;
        }

        application.setAttribute("OnlineNumber", onlineNumber);
        Const.ONLINE_USER_NUMBER = onlineNumber;
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        ServletContext application = event.getSession().getServletContext();

        Integer onlineNumber = (Integer) application.getAttribute("OnlineNumber");

        if (onlineNumber == null) {
            onlineNumber = 0;
        } else {
            --onlineNumber;
        }

        application.setAttribute("OnlineNumber", onlineNumber);
        Const.ONLINE_USER_NUMBER = onlineNumber;
    }
}

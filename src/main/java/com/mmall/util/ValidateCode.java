package com.mmall.util;

import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public class ValidateCode {
    //图片宽度
    private int width = 160;

    //图片高度
    private int height = 40;

    //验证码字符个数
    private int codeCount = 4;

    //干扰线个数
    private int lineCount = 20;

    //验证码
    private String code = null;

    //buffer Image 流
    private BufferedImage bufferImg = null;

    public String getCode() {
        return code;
    }

    public BufferedImage getBufferImg() {
        return bufferImg;
    }

    /**
     * 验证码字符集，去除1(数字), l(小写L), o(字母o), 0（数字0)
     */
    char[] codeSequence = {'a', 'b', 'c', 'd', 'e', 'f', 'g'
            , 'h', 'i', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't'
            , 'u', 'v', 'w', 'x', 'y', 'z', '2', '3', '4', '5', '6'
            , '7', '8', '9'};


    public ValidateCode() {

    }

    public ValidateCode(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public ValidateCode(int width, int height, int codeCount, int lineCount) {
        this.width = width;
        this.height = height;
        this.codeCount = codeCount;
        this.lineCount = lineCount;
    }

    public void createCode() {
        int x = 0, fontSize = 0, codeY = 0;
        int red = 0, green = 0, blue = 0;

        x = width / (codeCount + 1);    //每个字符的大小 = 图片宽 / (字符总数 + 1)
        fontSize = height - 4;  //字体大小 = 图片高度 - 6
        codeY = height - 6;

        //图像buffer
        bufferImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        //将buffer绘画实现
        Graphics2D g = bufferImg.createGraphics();

        //生成随机数
        Random random = new Random();

        //填充背景色
        g.setBackground(Color.WHITE);
        g.fillRect(0, 0, width, height);   //使填充色生效

        /**
         * 绘制验证码干扰线条
         */
        for (int i = 0; i < lineCount; i++) {
            int xs = random.nextInt(width);
            int xe = random.nextInt(width);
            int ys = random.nextInt(height);
            int ye = random.nextInt(height);

            //设置线条颜色
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
            g.setColor(new Color(red, green, blue));

            //创建线条
            g.drawLine(xs, ys, xe, ye);
        }

        /**
         * 生成验证码
         */
        Font font = new Font("Console", Font.PLAIN, fontSize);  //设置验证码字体
        g.setFont(font);
        StringBuffer randomCode = new StringBuffer();
        for (int i = 0; i < codeCount; i++) {
            //生成随机单个字符
            String strRand = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);

            //设置字符颜色
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
            g.setColor(new Color(red, green, blue));

            //绘制验证码
            g.drawString(strRand, (x * i + x), codeY);
            randomCode.append(strRand);
        }
        this.code = randomCode.toString();

        /**
         * 创建随机验证码
         */
        for (int i = 0; i < codeCount; i++) {
            String strRand = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
            randomCode.append(strRand);

        }
        g.dispose();    //释放绘图资源

    }

    public void write(String path) throws IOException {
        OutputStream os = new FileOutputStream(path);
        write(os);
    }

    /**
     * 直接写入response.getOutputStream图片
     * @param os
     * @throws IOException
     */
    public void write(OutputStream os) throws IOException {
        createCode();
        ImageIO.write(bufferImg, "png", os);
        os.close();
    }


    /**
     * 返回base64字符串
     * @return
     */
    public String getBase64FromImg() {
        createCode();
        byte[] bytes = buffer2byte(bufferImg);
        return Base64.encodeBase64String(bytes);
    }

    private byte[] buffer2byte(BufferedImage bufferImg) {
        ByteArrayOutputStream bytesOS = image2Stream(bufferImg);
        byte[] outToByteArray = bytesOS.toByteArray();
        return outToByteArray;
    }
    private ByteArrayOutputStream image2Stream(BufferedImage image) {
        ByteArrayOutputStream bytesOS = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "png", bytesOS);
            bytesOS.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bytesOS.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bytesOS;
    }


}

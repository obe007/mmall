package com.mmall.util;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * joda-time
 *
 *  str -> Date
 *  Date -> str
 */
public class DateTimeUitl {


    public static final String STANDARD_FORMAT = "yyyy年MM月dd日 HH:mm:ss";

    public static Date strToDate(String dateTimeStr, String formatStr) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(formatStr);
        try {
            DateTime dateTime = formatter.parseDateTime(dateTimeStr);
            return dateTime.toDate();
        } catch (Exception e) {
            System.out.println("格式错误,请检查你时间与格式化所用的符号是否一致: " + e);
        }

        return null;
    }

    public static String dateToStr(Date date, String formatStr) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);

        return dateTime.toString(formatStr);
    }

    public static Date strToDate(String dateTimeStr) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(STANDARD_FORMAT);
        try {
            DateTime dateTime = formatter.parseDateTime(dateTimeStr);
            return dateTime.toDate();
        } catch (Exception e) {
            System.out.println("格式错误,请检查你时间与格式化所用的符号是否一致: " + e);
        }

        return null;
    }

    public static String dateToStr(Date date) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);

        return dateTime.toString(STANDARD_FORMAT);
    }

}

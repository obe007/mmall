package com.mmall.util;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.mail.MailUtil;
import com.mmall.common.Const;
import com.mmall.common.ServerResponse;
import com.mmall.common.TokenCache;
import org.springframework.stereotype.Service;

@Service("mailTo")
public class MailTo {

    public ServerResponse forgetTokenToEmail(String username, String email, String forgetToken) {

        // 邮件标题
        String subject = "Jax系统密码找回功能";

        // 读取邮件模板
        String emailTplPath = MailTo.class.getResource("/config/template/forgetTokenTpl.html").getFile();
        FileReader fileReader = new FileReader(emailTplPath);
        String HTML = fileReader.readString();

        /**
         * 将模板变量替换, 变量是 {} , 会根据顺序替换
         */
        HTML = StrUtil.format(HTML, username, forgetToken);

        // forgetToken 写入瓜娃缓存
        TokenCache.setKey(Const.TOKEN_PREFIX + username, forgetToken);

        // 发送邮件
        MailUtil.sendHtml(email, subject, HTML);
        return ServerResponse.createBySuccess("验证码已发送至邮箱");
    }

}

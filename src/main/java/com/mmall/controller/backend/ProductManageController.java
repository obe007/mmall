package com.mmall.controller.backend;

import com.google.common.collect.Maps;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.pojo.User;
import com.mmall.service.IFileService;
import com.mmall.service.IProductService;
import com.mmall.service.IUserService;
import com.mmall.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/manage/product")
public class ProductManageController {

    @Autowired
    IUserService iUserService;

    @Autowired
    IProductService iProductService;

    @Autowired
    IFileService iFileService;

    /**
     * 新增or更新产品
     * @param session
     * @param product
     * @return
     */
    @RequestMapping("save.do")
    public ServerResponse productSave(HttpSession session, Product product) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iProductService.saveOrUpdateProduct(product);
        return response;
    }

    /**
     * 产品上下架
     * @param session
     * @param productId
     * @param status
     * @return
     */
    @RequestMapping("set_sale_status.do")
    public ServerResponse ProductSetSaleStatus(HttpSession session, Integer productId, Integer status) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iProductService.setSaleStatus(productId, status);
        return response;
    }

    /**
     * 产品详情
     * @param session
     * @param productId
     * @return
     */
    @RequestMapping("detail.do")
    public ServerResponse getDetail(HttpSession session, Integer productId) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iProductService.manageProductDetail(productId);
        return response;
    }

    /**
     * 所有产品list
     * @param session
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("list.do")
    public ServerResponse getList(HttpSession session, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iProductService.manageProductList(pageNum, pageSize);
        return response;
    }

    /**
     * 产品搜索
     * @param session
     * @param productId
     * @param productName
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("search.do")
    public ServerResponse search(HttpSession session, Integer productId, String productName, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iProductService.searchProduct(productId, productName, pageNum, pageSize);
        return response;

    }

    /**
     *
     * @param file
     * @param request
     * @return
     */
    @RequestMapping("upload.do")    //TODO 上传路径以及内容找不到不正确! 待修改
    public ServerResponse upload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }


        String path = request.getSession().getServletContext().getRealPath("upload");
        String targetFileName = iFileService.upload(file, path);
        String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
        Map fileMap = Maps.newHashMap();
        fileMap.put("uri", targetFileName);
        fileMap.put("url", url);

        return ServerResponse.createBySuccess(fileMap);
    }

}

package com.mmall.controller.backend;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator
 */
@RestController
@RequestMapping(value = "/manage/user")
public class UserManageController {

    @Autowired
    IUserService iUserService;

    /**
     * 统计在线用户数量
     * @param session
     * @return
     */
    @RequestMapping("get_user_number.do")
    public ServerResponse getUserNumber(HttpSession session) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        if (Const.ONLINE_USER_NUMBER == null) {
            return ServerResponse.createByError();
        }

        return ServerResponse.createBySuccess(Const.ONLINE_USER_NUMBER);
    }

    /**
     * 后台用户登录
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    public ServerResponse login(String username, String password, HttpSession session) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return ServerResponse.createByErrorMessage("用户名或密码不能为空");
        }
        username = username.trim();
        ServerResponse response = iUserService.login(username, password);
        if (response.isSuccess()) {
            session.setAttribute(Const.CURRENT_USER, response.getData());
        }

        return response;
    }

}

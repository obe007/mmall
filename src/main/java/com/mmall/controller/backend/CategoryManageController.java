package com.mmall.controller.backend;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Category;
import com.mmall.pojo.User;
import com.mmall.service.ICategoryService;
import com.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Administrator
 */
@RestController
@RequestMapping("/manage/category")
public class CategoryManageController {

    @Autowired
    private ICategoryService iCategoryService;

    @Autowired
    private IUserService iUserService;  //从bean中把IUserService的实现类(Impl)注入到当前Controller层

    @RequestMapping("get_category.do")
    public ServerResponse getCategory(HttpSession session, @RequestParam(value = "parentId", defaultValue = "0") Integer parentId) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iCategoryService.getCategory(parentId);
        return response;
    }

    @RequestMapping(value = "add_category.do", method = RequestMethod.POST)
    public ServerResponse addCategory(HttpSession session, String categoryName, @RequestParam(value = "parentId", defaultValue = "0") Integer parentId) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iCategoryService.addCategory(parentId, categoryName);
        return response;
    }

    @RequestMapping(value = "set_category_name.do", method = RequestMethod.POST)
    public ServerResponse setCategoryName(HttpSession session, String categoryName, Integer categoryId) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iCategoryService.updateCategoryName(categoryId, categoryName);
        return response;
    }

    @RequestMapping(value = "get_children_parallel_category.do", method = RequestMethod.POST)
    public ServerResponse getChildrenParallelCategory(HttpSession session, @RequestParam(value = "categoryId", defaultValue = "0") Integer categoryId) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iCategoryService. getChildrenParallelCategory(categoryId);
        return response;
    }

    @RequestMapping(value = "get_deep_category.do", method = RequestMethod.POST)
    public ServerResponse getDeepCategory(HttpSession session, @RequestParam(value = "categoryId", defaultValue = "0") Integer categoryId) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        if (!iUserService.checkAdminRole(currentUser).isSuccess()) {    //取非值 不是管理员的提示错误
            return ServerResponse.createByErrorMessage("无权限操作,你不是管理员");
        }

        ServerResponse response = iCategoryService. selectCategoryAndChildrenById(categoryId);
        return response;
    }
}

package com.mmall.controller.base;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("api")
public class AuthController extends BaseController{

    /**
     * 生成图片验证码, 验证码存入attribute("validateCode") 必须使用post方法才能存
     * @return
     * @throws Exception
     * @version 3.0.0
     */
    @RequestMapping(value = "/get_validate_code.do", method = RequestMethod.POST)
    public void getValidateCode(HttpServletResponse response, HttpSession session) throws Exception {

        super.generateImgCode(response, session);
    }

}

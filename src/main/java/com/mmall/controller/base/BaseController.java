package com.mmall.controller.base;

import com.mmall.common.Const;
import com.mmall.util.ValidateCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class BaseController {

    private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;
    protected String language;

    protected void generateImgCode(HttpServletResponse response, HttpSession session) throws Exception {

        // 设置响应的类型格式为图片格式
//        response.setContentType("image/png");
        response.setContentType("text/plain;charset=UTF-8"); //Base64 字符串格式
        response.setCharacterEncoding("UTF-8");


        // 禁止图像缓存
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", -1);


        // 验证码生成
        ValidateCode vCode = new ValidateCode();
//        vCode.write(response.getOutputStream());  //直接返回图片方法
        response.getWriter().print(vCode.getBase64FromImg());   //base64


        // 写入session中
        session.setAttribute(Const.VALIDATE_CODE, vCode.getCode());

        // 不适用session.getOutputStream必须注释
//        response.getOutputStream().flush();
//        response.getOutputStream().close();
    }

}

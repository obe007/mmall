package com.mmall.controller.portal;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2018/7/5.
 */
@RestController // @Controll + 所有内容 @ResponseBody
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService iUserService;  //IUserService 和 UserServiceImpl这个文件@Service("iUserService")一样就是自动装配到bean

    /**
     * 用户名登录
     * service --> mybatis --> dao
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    public ServerResponse<User> login(String username, String password, String validateCode, HttpSession session) {
        if (StringUtils.isBlank(username) || StringUtils.isEmpty(password)) {
            return ServerResponse.createByErrorMessage("用户名或密码不能为空");
        }
        username = username.trim(); //用户名去两边空格,密码不行空格也算密码

        String code = (String) session.getAttribute(Const.VALIDATE_CODE);
        if (!StringUtils.equals(validateCode, code)) {
                return ServerResponse.createByErrorMessage("验证码不正确");
        }

        ServerResponse response = iUserService.login(username, password);
        if (response.isSuccess()) {
            session.setAttribute(Const.CURRENT_USER, response.getData());
        }

        return response;
    }

    @RequestMapping("logout.do")
    public ServerResponse<String> logout(HttpSession session) {
        session.removeAttribute(Const.CURRENT_USER);
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "register.do", method = RequestMethod.POST)
    public ServerResponse<String> register(User user) {
        ServerResponse<String> response = iUserService.register(user);
        return response;
    }

    @RequestMapping(value = "check_valid.do", method = RequestMethod.POST)
    public ServerResponse<String> checkValid(String str, String type) {
        ServerResponse response = iUserService.checkValid(str, type);
        return response;
    }

    @RequestMapping(value = "get_user_info.do", method = RequestMethod.POST)
    public ServerResponse<Object> getUserInfo(HttpSession session) {
        Object user = session.getAttribute(Const.CURRENT_USER);
        if (user == null) return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
        return ServerResponse.createBySuccess(user);
    }

    @RequestMapping(value = "forget_get_question.do", method = RequestMethod.POST)
    public ServerResponse<String> forgetGetQuestion(String username) {
        ServerResponse response = iUserService.selectQuestion(username);
        return response;
    }

    @RequestMapping(value = "forget_check_answer.do", method = RequestMethod.POST)
    public ServerResponse<String> forgetCheckAnswer(String username, String question, String answer) {
        ServerResponse response = iUserService.checkAnswer(username, question, answer);
        return response;
    }

    /**
     * 通过邮箱找回forgetToken令牌
     * @param username
     * @return
     */
    @RequestMapping(value = "forgetToken_send_Email.do", method = RequestMethod.POST)
    public ServerResponse<String> forgetTokenSendEmail(String username) {
        if (StringUtils.isBlank(username)) {
            return ServerResponse.createByErrorMessage("用户名填写错误");
        }

        ServerResponse response = iUserService.forgetTokenToEmail(username);
        return response;
    }

    /**
     * 修改密码 => 填入（用户名， 新密码， 正确的token）
     * @param username
     * @param passwordNew
     * @param forgetToken
     * @return
     */
    @RequestMapping(value = "forget_reset_password.do", method = RequestMethod.POST)
    public ServerResponse<String> forgetResetPassword(String username, String passwordNew, String forgetToken) {
        ServerResponse response = iUserService.forgetResetPassword(username, passwordNew, forgetToken);
        return response;
    }

    @RequestMapping(value = "reset_password.do", method = RequestMethod.POST)
    public ServerResponse<String> resetPassword(HttpSession session, String passwordOld, String passwordNew) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        ServerResponse response = iUserService.resetPassword(user, passwordOld, passwordNew);
        session.removeAttribute(Const.CURRENT_USER);
        return response;
    }

    @RequestMapping(value = "update_information.do", method = RequestMethod.POST)
    public ServerResponse<User> updateInformation(HttpSession session, String email, String phone, String question, String answer) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        ServerResponse<User> response = iUserService.updateInformation(user, email, phone, question, answer);

        if (response.isSuccess()) {
            session.setAttribute(Const.CURRENT_USER, response.getData());
        }

        return response;
    }

    @RequestMapping(value = "get_information.do", method = RequestMethod.POST)
    public ServerResponse<User> getInformation(HttpSession session) {   //不明白为什么要多此一举,直接从session中获取信息不行么
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);

        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,无法获取当前用户信息!");
        }

        ServerResponse<User> response = iUserService.getInformation(currentUser.getId());

        return response;
    }




}

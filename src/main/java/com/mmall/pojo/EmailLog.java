package com.mmall.pojo;

import java.util.Date;

public class EmailLog {
    private Integer id;

    private Integer userId;

    private String email;

    private String type;

    private String code;

    private String content;

    private Integer validMinute;

    private Date sendTime;

    public EmailLog(Integer id, Integer userId, String email, String type, String code, String content, Integer validMinute, Date sendTime) {
        this.id = id;
        this.userId = userId;
        this.email = email;
        this.type = type;
        this.code = code;
        this.content = content;
        this.validMinute = validMinute;
        this.sendTime = sendTime;
    }

    public EmailLog() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getValidMinute() {
        return validMinute;
    }

    public void setValidMinute(Integer validMinute) {
        this.validMinute = validMinute;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }
}
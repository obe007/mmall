package com.mmall.dao;

import com.mmall.pojo.EmailLog;

public interface EmailLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(EmailLog record);

    int insertSelective(EmailLog record);

    EmailLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EmailLog record);

    int updateByPrimaryKey(EmailLog record);

    /**
     * 查找EmailLog 通过email
     * @param email
     * @return
     */
    EmailLog selectEmailLogByEmail(String email);

    /**
     * 更新发送时间 根据email
     * @param record
     * @return
     */
    int updateSendTimeByEmail(EmailLog record);


}
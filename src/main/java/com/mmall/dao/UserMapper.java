package com.mmall.dao;

import com.mmall.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int updatePasswordByUsername(@Param("username") String username, @Param("passwordNew") String passwordNew);

    int checkUsername(String username);

    int checkEmail(String email);

    int checkAnswer(@Param("username") String username, @Param("question") String question, @Param("answer") String answer);

    User selectLgoin(@Param("username") String username, @Param("password") String password);

    String selectQuestionByUsername(String username);

    String selectEmailByUsername(String username);

    /**
     * 查询整个user, 通过username
     * @param username
     * @return
     */
    User selectUserByUsername(String username);
}
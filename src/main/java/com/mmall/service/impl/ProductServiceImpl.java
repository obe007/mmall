package com.mmall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Category;
import com.mmall.pojo.Product;
import com.mmall.service.IProductService;
import com.mmall.util.DateTimeUitl;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.ProductDetailVo;
import com.mmall.vo.ProductListVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("iProductService")
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 新增or更新产品
     * @param product
     * @return
     */
    public ServerResponse saveOrUpdateProduct(Product product) {    //TODO 接口插入中文乱码 前端插入正常
        if (product == null) {
            return ServerResponse.createByErrorMessage("新增或更新产品参数不正确");
        }

        if (StringUtils.isNotBlank(product.getSubImages()) && product.getMainImage() == null) {
            String[] subImagesArray = product.getSubImages().split(",");
            if (subImagesArray.length > 0) {
                product.setMainImage(subImagesArray[0]);
            }
        }

        if (product.getId() != null) {
            int updateCount = productMapper.updateByPrimaryKeySelective(product);
            if (updateCount > 0) {
                return ServerResponse.createBySuccess("更新产品成功");
            }
            return ServerResponse.createByErrorMessage("更新产品失败");
        } else {
            int resultCount = productMapper.insert(product);
            if (resultCount > 0) {
                return ServerResponse.createBySuccess("新增产品成功");
            }
            return ServerResponse.createByErrorMessage("新增产品失败");
        }
    }

    public ServerResponse setSaleStatus(Integer productId, Integer status) {
        if (productId == null || status == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(), "请填写完整产品的ID和状态");
        }

        Product product = new Product();
        product.setId(productId);
        product.setStatus(status);

        int resultCount = productMapper.updateByPrimaryKeySelective(product);
        if (resultCount > 0) {
            return ServerResponse.createBySuccessMessage("修改产品状态成功");
        }

        return ServerResponse.createByErrorMessage("修改产品状态失败");
    }

    public ServerResponse<ProductDetailVo> manageProductDetail(Integer productId) {
        if (productId == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }

        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null) {
            return ServerResponse.createByErrorMessage("产品已下架或不存在");
        }
        ProductDetailVo productDetailVo = assembleProductDetailVo(product);
        //VO对象--value object
        //pojo ->bo(business object) -> vo(view object)


        return ServerResponse.createBySuccess(productDetailVo);
    }

    private ProductDetailVo assembleProductDetailVo(Product product) {
        ProductDetailVo productDetailVo = new ProductDetailVo();

        productDetailVo.setId(product.getId());
        productDetailVo.setCategoryId(product.getCategoryId());
        productDetailVo.setName(product.getName());
        productDetailVo.setSubImage(product.getMainImage());
        productDetailVo.setMainImage(product.getMainImage());
        productDetailVo.setDetail(product.getDetail());
        productDetailVo.setPrice(product.getPrice());
        productDetailVo.setStock(product.getStock());
        productDetailVo.setStatus(product.getStatus());

        //imageHost
        productDetailVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix","E:\\Unit Testing\\WEB\\img"));

        //parentCategoryId
        Category category = categoryMapper.selectByPrimaryKey(product.getCategoryId());
        if (category == null) {
            productDetailVo.setParentCategoryId(0); //默认根节点
        } else {
            productDetailVo.setParentCategoryId(category.getParentId());
        }

        //CreateTime & UpdateTime
        productDetailVo.setCreateTime(DateTimeUitl.dateToStr(product.getCreateTime()));
        productDetailVo.setUpdateTime(DateTimeUitl.dateToStr(product.getUpdateTime()));

        return productDetailVo;
    }

    public ServerResponse<PageInfo> manageProductList(int pageNum, int pageSize) {
         //startPage -- start
         //填充自己sql查询逻辑
         //pageHelper -- 收尾
        PageHelper.startPage(pageNum, pageSize);

        List<Product> productList = productMapper.selectProductList();
        if (productList == null) {
            return ServerResponse.createByErrorMessage("无法获取产品列表");
        }

        List<ProductListVo> productListVoList = Lists.newArrayList();
        for (Product productItem : productList) {   //List<Product> 转换成 List<ProductListVo>
            ProductListVo productListVoItem = assembleProductListVo(productItem);
            productListVoList.add(productListVoItem);
        }

        if (productListVoList == null) {
            return ServerResponse.createByErrorMessage("转换错误");
        }

        PageInfo pageResult = new PageInfo(productListVoList);

        return ServerResponse.createBySuccess(pageResult);
    }

    /**
     * ProductListVo
     * @param product
     * @return
     */
    private ProductListVo assembleProductListVo(Product product) {
        ProductListVo productListVo = new ProductListVo();

        productListVo.setId(product.getId());
        productListVo.setCategoryId(product.getCategoryId());
        productListVo.setName(product.getName());
        productListVo.setSubtitle(product.getSubtitle());
        productListVo.setMainImage(product.getMainImage());
        productListVo.setStatus(product.getStatus());
        productListVo.setPrice(product.getPrice());

        //imageHost
        productListVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix","E:\\Unit Testing\\WEB\\img"));
        return productListVo;
    }

    public ServerResponse<PageInfo> searchProduct(Integer productId, String productName, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        List<Product> productList = productMapper.selectByNameAndProductId(productId, productName);
        List<ProductListVo> productListVoList = Lists.newArrayList();

        for (Product productItem : productList) {
            ProductListVo productListVoItem = assembleProductListVo(productItem);
            productListVoList.add(productListVoItem);
        }

        PageInfo pageResult = new PageInfo(productListVoList);
        return ServerResponse.createBySuccess(pageResult);
    }


}

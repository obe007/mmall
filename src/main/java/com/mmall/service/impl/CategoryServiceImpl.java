package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.pojo.Category;
import com.mmall.service.ICategoryService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;

/**
 * Created by Administrator
 */
@Service("iCategoryService")
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private CategoryMapper  categoryMapper;

    private Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    public ServerResponse<List<Category>> getCategory(Integer parentId) {
        if (parentId == null) {
            return ServerResponse.createByErrorMessage("添加品类的ID值不可为空");
        }

        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(parentId);
        if (CollectionUtils.isEmpty(categoryList)) {
            logger.error("通过parentId未找到品类");
        }

        return ServerResponse.createBySuccess(categoryList);
    }

    public ServerResponse addCategory(Integer parentId, String categoryName) {
        if (StringUtils.isBlank(categoryName)) {
            return ServerResponse.createByErrorMessage("品类的名字非法");
        }
        if (parentId == null) {
            return ServerResponse.createByErrorMessage("添加品类的ID值不可为空");
        }

        Category category = new Category();
        category.setName(categoryName);
        category.setParentId(parentId);
        category.setStatus(true);   //这个分类为可用

        int rowCount = categoryMapper.insert(category);
        if (rowCount < 1) {
            return ServerResponse.createByErrorMessage("添加品类失败");
        }

        return ServerResponse.createBySuccessMessage("添加品类成功");
    }

    public ServerResponse updateCategoryName(Integer categoryId, String categoryName) {
        if (StringUtils.isBlank(categoryName)) {
            return ServerResponse.createByErrorMessage("品类的名字非法");
        }
        if (categoryId == null) {
            return ServerResponse.createByErrorMessage("添加品类的ID值不可为空");
        }

        Category category = new Category();
        category.setId(categoryId);
        category.setName(categoryName);

        int updateCount = categoryMapper.updateByPrimaryKeySelective(category);
        if (updateCount < 1) {
            return ServerResponse.createByErrorMessage("更新品类名字失败");
        }

        return ServerResponse.createBySuccessMessage("更新品类名字成功");
    }

    public ServerResponse<List<Category>> getChildrenParallelCategory(Integer categoryId) {
        if (categoryId == null) {
            return ServerResponse.createByErrorMessage("添加品类的ID值不可为空");
        }

        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
        if (CollectionUtils.isEmpty(categoryList)) {
            logger.error("通过parrentId未找到分类子节点");
        }

        return ServerResponse.createBySuccess(categoryList);
    }

    /**
     * 递归查找节点以及子节点的ID
     * @param categoryId
     * @return
     */
    public ServerResponse selectCategoryAndChildrenById(Integer categoryId) {
        if (categoryId == null) {
            return ServerResponse.createByErrorMessage("添加品类的ID值不可为空");
        }

        Set<Category> categorySet = Sets.newHashSet();
        findChildrenCategory(categorySet, categoryId);

        List<Integer> categoryIdList = Lists.newLinkedList();
        for (Category categoryItem : categorySet) {
            categoryIdList.add(categoryItem.getId());
        }

//        java.util.Collections.sort(categoryIdList); //排序

        return ServerResponse.createBySuccess(categoryIdList);
    }

    /**
     * 递归查找category以及子节点
     * @param categorySet
     * @param categoryId
     * @return
     */
    private Set<Category> findChildrenCategory(Set<Category> categorySet, Integer categoryId) {
        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if (category != null) {
            categorySet.add(category);
        }

        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
        for (Category categoryItem : categoryList) {
            findChildrenCategory(categorySet, categoryItem.getId());
        }

        return categorySet;
    }

}

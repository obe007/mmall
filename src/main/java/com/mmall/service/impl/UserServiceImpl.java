package com.mmall.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.common.TokenCache;
import com.mmall.dao.EmailLogMapper;
import com.mmall.dao.UserMapper;
import com.mmall.model.constants.EmailTypeEnum;
import com.mmall.pojo.EmailLog;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.MD5Util;
import com.mmall.util.MailTo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by Administrator on 2018/7/5.
 */
@Service("iUserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private EmailLogMapper emailLogMapper;

    @Autowired
    private MailTo mailTo;

    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override   //当类继承接口，必须强制实现接口中有的。这时候会出现 @Override
    public ServerResponse<User> login(String username, String password) {
        int resultCount = userMapper.checkUsername(username);
        if (resultCount == 0) {
            return ServerResponse.createByErrorMessage("用户名不存在");
        }

        //MD5加密登录
        String md5Password = MD5Util.MD5EncodeUtf8(password);

        User user = userMapper.selectLgoin(username, md5Password);
        if (user == null) {
            return ServerResponse.createByErrorMessage("密码错误");
        }

        user.setPassword(StringUtils.EMPTY);    //密码置空，不让前端获得密码
        return ServerResponse.createBySuccess("登录成功", user);
    }

    @Override
    public ServerResponse<String> register(User user) {

        /*int resultCount = userMapper.checkUsername(user.getUsername());   //被checkValid()复用取代
        if (resultCount > 0) {
            return ServerResponse.createByErrorMessage("用户名已存在");
        }
        resultCount = userMapper.checkEmail(user.getEmail());
        if (resultCount > 0) {
            return ServerResponse.createByErrorMessage("邮箱已存在");
        }*/

        ServerResponse validResponse = this.checkValid(user.getUsername(), Const.USERNAME);
        if (!validResponse.isSuccess()) {
            return validResponse;
        }

        validResponse = this.checkValid(user.getEmail(), Const.EMAIL);
        if (!validResponse.isSuccess()) {
            return validResponse;
        }

        user.setRole(Const.Role.ROLE_CUSTOMER); //设置管理级别为普通
        user.setPassword(MD5Util.MD5EncodeUtf8(user.getPassword()));    //MD5加密


        int resultCount = userMapper.insert(user);  //密码插入数据库
        if (resultCount == 0) {
            return ServerResponse.createByErrorMessage("注册失败");
        }

        return ServerResponse.createBySuccessMessage("注册成功");
    }

    public ServerResponse<String> checkValid(String str, String type) {
        if (!StringUtils.isNotBlank(type)) {  //type不为空开始校验
            return ServerResponse.createByErrorMessage("类型为空");
        }
        if (!StringUtils.isNotBlank(str)) {  //str不为空开始校验
            return ServerResponse.createByErrorMessage("值为空");
        }

        int resultCount;
        if (Const.USERNAME.equals(type)) {
            resultCount = userMapper.checkUsername(str);
            if (resultCount > 0) {
                return ServerResponse.createByErrorMessage("用户名已存在");
            }
        }

        if (Const.EMAIL.equals(type)) {
            resultCount = userMapper.checkEmail(str);
            if (resultCount > 0) {
                return ServerResponse.createByErrorMessage("邮箱已存在");
            }
        }

        if (!Const.EMAIL.equals(type) && !Const.USERNAME.equals(type)) {    //都不符合已有类型
            return ServerResponse.createByErrorMessage("类型无效");
        }

        return ServerResponse.createBySuccessMessage("校验成功");
    }

    public ServerResponse<String> selectQuestion(String username) {
        ServerResponse validResponse = this.checkValid(username, Const.USERNAME);
        if (StringUtils.isBlank(username)) {
            return ServerResponse.createByErrorMessage("传入用户名为空");
        }

        if (!validResponse.getMsg().equals("用户名已存在")) {
            return ServerResponse.createByErrorMessage("用户名不存在");
        }

        String question = userMapper.selectQuestionByUsername(username);
        if (StringUtils.isBlank(question)) {
            return ServerResponse.createByErrorMessage("该用户未设置找回密码问题");
        }

        return ServerResponse.createBySuccess(question);
    }


    public ServerResponse<String> checkAnswer(String username, String question, String answer) {
        ServerResponse validResponse = this.checkValid(username, Const.USERNAME);
        if (!validResponse.getMsg().equals("用户名已存在")) {
            return ServerResponse.createByErrorMessage("用户名不存在");
        }

        if (StringUtils.isBlank(question) || StringUtils.isBlank(answer)) {
            return ServerResponse.createByErrorMessage("问题或答案不能为空");
        }

        int resultCount = userMapper.checkAnswer(username, question, answer);
        if (resultCount < 1) {
            return ServerResponse.createByErrorMessage("问题答案错误");
        }

        String forgetToken = RandomUtil.randomStringUpper(10);
        TokenCache.setKey(Const.TOKEN_PREFIX + username, forgetToken);
        return ServerResponse.createBySuccess(forgetToken);
    }

    public ServerResponse forgetTokenToEmail(String username) {

        int userCount = userMapper.checkUsername(username);
        if (userCount <= 0) {
            return ServerResponse.createByErrorMessage("用户名不存在");
        }

        // 查询整个User信息
        User userInfo = userMapper.selectUserByUsername(username);
        if (StringUtils.isBlank(userInfo.getEmail())) {
            return ServerResponse.createByErrorMessage("未填写邮箱,请找客服申诉");
        }

        String forgetToken = RandomUtil.randomStringUpper(10);  //生成token验证码
        // 邮箱间隔时间检测
        EmailLog emailLog = emailLogMapper.selectEmailLogByEmail(userInfo.getEmail());
        if (emailLog == null) { //第一次使用找回密码
            emailLog = new EmailLog();
            emailLog.setUserId(userInfo.getId());
            emailLog.setEmail(userInfo.getEmail());
            emailLog.setType(EmailTypeEnum.findReg.toString());
            emailLog.setCode(forgetToken);
            emailLog.setValidMinute(60); //设置间隔时间60秒  这个当做秒
            emailLog.setSendTime(new Date());   //设置发送时间
            int insertCount = emailLogMapper.insertSelective(emailLog);
            if (insertCount < 1) {
                logger.error("forgetTokenToEmail()", "第一次找回密码数据库插入错误");
                return ServerResponse.createByError("系统错误");
            }
            ServerResponse sendMail = mailTo.forgetTokenToEmail(username, userInfo.getEmail(), forgetToken);
            return sendMail;

        } else {
            long validMinute = emailLog.getValidMinute().longValue() * 1000;   //间隔时间 1000是毫秒转成秒
            long sendTime = emailLog.getSendTime().getTime();           //发送时间
            long nowTime = new Date().getTime();                        //当前时间

            boolean canSend = (nowTime - sendTime) >= validMinute;
            if (canSend == false) {
                String time = (validMinute - (nowTime - sendTime)) / 1000 + "";
                JSONObject sendEmailJson = JSONUtil.createObj();
                sendEmailJson.put("time_remaining", time);
                return ServerResponse.createByError("请稍后再尝试", sendEmailJson);
            }

            /**
             * 调用邮箱发送forgetToken令牌
             */
            ServerResponse sendMail = mailTo.forgetTokenToEmail(username, userInfo.getEmail(), forgetToken);

            // 更新发送时间 和 验证码
            if(sendMail.isSuccess()) {
                emailLog = new EmailLog();
                emailLog.setSendTime(new Date());
                emailLog.setCode(forgetToken);
                emailLog.setEmail(userInfo.getEmail());

                int updateCount = emailLogMapper.updateSendTimeByEmail(emailLog);
                if (updateCount < 1) {
                    logger.error("forgetTokenToEmail()", "发送邮件更新发送时间错误");
                    return ServerResponse.createByErrorMessage("系统错误");
                }
            }
            return sendMail;
        }
    }

    public ServerResponse<String> forgetResetPassword(String username, String passwordNew, String forgetToken) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(passwordNew)) {
            return ServerResponse.createByErrorMessage("用户名密码不可为空!");
        }

        if (StringUtils.isBlank(forgetToken)) {
            return ServerResponse.createByErrorMessage("参数错误,Token需要传递");
        }

        if (!StringUtils.equals(forgetToken, TokenCache.getKey(Const.TOKEN_PREFIX + username))) {
            return ServerResponse.createByErrorMessage("token错误,请重新回答问题");
        }

        String md5Password = MD5Util.MD5EncodeUtf8(passwordNew);    //MD5加密
        int rowCount = userMapper.updatePasswordByUsername(username, md5Password);
        if (rowCount < 1) {
            return ServerResponse.createByErrorMessage("重置密码失败,请重试");
        }

        return ServerResponse.createBySuccessMessage("修改密码成功");

    }

    public ServerResponse<String> resetPassword(User user, String passwordOld, String passwordNew) {
        if (user == null) {
            return ServerResponse.createByErrorMessage("请登录您的账户");
        }

        if (StringUtils.isBlank(passwordOld) || StringUtils.isBlank(passwordNew)) {
            return ServerResponse.createByErrorMessage("密码不能为空");
        }

        String md5password = MD5Util.MD5EncodeUtf8(passwordOld);
        User resultCount = userMapper.selectLgoin(user.getUsername(), md5password);
        if (resultCount == null) {
            return ServerResponse.createByErrorMessage("旧密码错误,请重试");
        }

        md5password = MD5Util.MD5EncodeUtf8(passwordNew);
        int updateCount = userMapper.updatePasswordByUsername(user.getUsername(), md5password);
        if (updateCount < 1) {
            return ServerResponse.createByErrorMessage("修改密码失败,请重试");
        }

        return ServerResponse.createBySuccessMessage("密码修改成功");
    }

    public ServerResponse<User> updateInformation(User user, String email, String phone, String question, String answer) {
        if (user == null) {
            return ServerResponse.createByErrorMessage("请登录您的账户");
        }

        int emailCount = userMapper.checkEmail(email);
        if (StringUtils.equals(email, user.getEmail()) || StringUtils.equals(email, null)) {    //邮箱参数不填写或是本身则直接生效
            emailCount = 0;
            email = null;   //优化:直接设为空, 少执行一次数据库读写
        }
        if (emailCount > 0) {
            return ServerResponse.createByErrorMessage("邮箱已存在,请更换邮箱后重试");
        }

        /**
         * 不为空才更新信息,为空使用默认信息赋值所以不变
         */
        if (StringUtils.isNotBlank(email)) user.setEmail(email);
        if (StringUtils.isNotBlank(phone)) user.setPhone(phone);
        if (StringUtils.isNotBlank(question)) user.setQuestion(question);
        if (StringUtils.isNotBlank(answer)) user.setAnswer(answer);

        int updateCount = userMapper.updateByPrimaryKeySelective(user);
        if (updateCount < 1) {
            return ServerResponse.createByErrorMessage("更新信息失败,请重试");
        }

        return ServerResponse.createBySuccess("更新个人信息成功", user);
    }

    public ServerResponse<User> getInformation(Integer userId) {
        if (userId == null) {
            return ServerResponse.createByErrorMessage("用户ID为空,错误");
        }

        User userInformation = userMapper.selectByPrimaryKey(userId);
        if (userInformation == null) {
            ServerResponse.createByErrorMessage("找不到当前用户的信息");
        }

        userInformation.setPassword(StringUtils.EMPTY); //密码制空

        return ServerResponse.createBySuccess(userInformation);
    }


    //backend

    /**
     * 校验是否为管理员
     * @param currentUser
     * @return
     */
    public ServerResponse checkAdminRole(User currentUser) {
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录您的账户");
        }

        if (currentUser.getRole().intValue() != Const.Role.ROLE_ADMIN) {    //用intValue是因为integer 转成 int类型
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), "您不是管理员!");
        }

        return ServerResponse.createBySuccess();
    }

}

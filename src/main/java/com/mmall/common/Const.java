package com.mmall.common;

public class Const {

    public static final String TOKEN_PREFIX = "token_";

    public static final String CURRENT_USER = "currentUser";

    public static final String VALIDATE_CODE = "validateCode";

    public static final String USERNAME = "username";
    public static final String EMAIL = "email";


    /**
     * 全局变量
     */
    public static Integer ONLINE_USER_NUMBER = 0;   //在线用户数量

    public interface Role {
        int ROLE_CUSTOMER = 0; //普通用户
        int ROLE_ADMIN = 1; //管理员
    }
}
